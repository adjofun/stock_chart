import akka.NotUsed
import akka.stream.DelayOverflowStrategy
import akka.stream.scaladsl.{DelayStrategy, Source}
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.SpanSugar.convertIntToGrainOfTime

import java.net.URL
import java.nio.file.{Files, Path, Paths}
import java.time.Instant
import java.util.stream.Stream
import scala.util.Random

final class StockChartTest extends AnyFlatSpec with MockFactory {
  private val random = new Random()
  private val jitter = { random.between(200, 800) }

  def randomTrades(amount: Int): Source[Trade, NotUsed] = {
    Source(0 until amount)
      .delayWith(() => DelayStrategy.fixedDelay(jitter.milliseconds), DelayOverflowStrategy.backpressure)
      .map((_: Int) => Trade(Instant.now, random.between(42, 1337)))
  }

  private val tradeFileResource = Option(getClass.getClassLoader.getResource("trades.csv"))
    .map((r: URL) => Paths.get(r.toURI))

  def tradeFile(): Source[Trade, NotUsed] = {
    val ifEmpty = () => Stream.empty[String]()
    val ifFull = (p: Path) => () => Files.lines(p)
    val linesSupplier = tradeFileResource.fold(ifEmpty)(ifFull)
    Source.fromJavaStream(linesSupplier) //TODO: underlying file should be closed when stream closes
      .delayWith(() => DelayStrategy.fixedDelay(jitter.milliseconds), DelayOverflowStrategy.backpressure)
      .map((priceStr: String) => Trade(Instant.now, BigDecimal(priceStr)))
  }




}
