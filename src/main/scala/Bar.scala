import java.time.Instant
import scala.collection.mutable

final case class Candle(startTime: Instant, open: BigDecimal, high: BigDecimal, low: BigDecimal, close: BigDecimal) {
  override def toString: String = s"[$startTime] - <$open [⤓$low ~ ⤒$high] $close>"
}
object Candle {
  def from[B <: mutable.Buffer[Trade]](trades: B, startTime: Instant): Candle = {
    val open = trades.minBy(_.time)
    val high = trades.maxBy(_.price)
    val low = trades.minBy(_.price)
    val close = trades.maxBy(_.time)
    Candle(startTime, open.price, high.price, low.price, close.price)
  }
}
