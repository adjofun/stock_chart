import akka.NotUsed
import akka.actor.Cancellable
import akka.stream.scaladsl.{Keep, Source}

import java.time.Instant
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.{DurationInt, FiniteDuration}

final class StockChart(trades: Source[Trade, NotUsed]) {
  def createBars(candleWidth: FiniteDuration): Source[Bar, Cancellable] = {
    val pacemaker = Source.tick(0.second, candleWidth, ())
      .map((_: Unit) => Instant.now())
    trades.conflateWithSeed((t: Trade) => ArrayBuffer[Trade](t))(_ += _)
      .zipWithMat(pacemaker)(Bar.from)(Keep.right)
  }
}
