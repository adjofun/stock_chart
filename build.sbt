name := "stock_chart"
version := "0.1"
scalaVersion := "2.13.6"

val AkkaVersion = "2.6.16"
val ScalaTestVersion = "3.2.9"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion,
)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-stream" % AkkaVersion % Test,
  "com.typesafe.akka" %% "akka-stream-testkit" % AkkaVersion % Test,
  "org.scalactic" %% "scalactic" % ScalaTestVersion % Test,
  "org.scalatest" %% "scalatest" % ScalaTestVersion % Test,
  "org.scalatest" %% "scalatest-funsuite" % "3.2.9" % Test
)
